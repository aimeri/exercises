const allWords = [];
fetch('https://api.datamuse.com/words?topics=horror&max=1000')
    .then(response => response.json())
    .then(function(wordsArray){
        for(let i = 0; i < wordsArray.length; i++){
            if (wordsArray[i].word.length <= 7){
                allWords.push(wordsArray[i].word);
            }
        }
        createWordTiles(chooseWord(allWords));
    })
    .catch(function(error){console.log('Request failed', error)});



const chooseWord = function (arrayOfWords) {
    const listLen = arrayOfWords.length;
    const wordChosen = arrayOfWords[Math.floor(Math.random() * listLen)];
        
    return wordChosen;
};

const createWordTiles = function (choosenWord) {
    const container = document.body.getElementsByClassName('container')[0];
    const containerWidth = getComputedStyle(container).width;
    const stylesheet = document.styleSheets[0];
    const tileSize = Math.floor(containerWidth.slice(0,-2)/choosenWord.length);
    const fontSize = Math.floor(containerWidth.slice(0,-2)*0.5);
    
    const tileStyle = '.letters { width: ' + tileSize + 'px}';
    const fontStyle = '.letters { font-size: ' + fontSize + '%}';
    
    
    stylesheet.insertRule(tileStyle,0);
    stylesheet.insertRule(fontStyle,0);
    
    const letters = choosenWord.split('');
    for (let i = 0; i < letters.length; i++){
        const letterContainer = document.createElement('div');
        letterContainer.classList.add('letters');
        letterContainer.classList.add(letters[i]);
        letterContainer.innerHTML = '<p>' + letters[i] + '</p>';
        container.appendChild(letterContainer);
    }
    const lettersDiv = document.body.getElementsByClassName('letters')[0];
    const containerHeight = Math.floor((getComputedStyle(lettersDiv).height).slice(0,-2)) + 40 ;
    const containerStyle = '#word { height: ' + containerHeight + 'px}';
    stylesheet.insertRule(containerStyle,0);
};