# exercises
Three beginner exercises to practice DOM manipulation


# Word-guessing game

This exercise creates a game that lets a player guess letters in a word. When the game starts, the div#word should be populated with div.letter tiles, one for each letter in the target word. As a player makes correct guesses, the tiles in the target word should be updated to show the correct guesses. As a player makes incorrect guesses, the scoreboard should be updated to show each incorrect guess; also, the number of guesses remaining should be decremented. After seven incorrect guesses, the player loses. The player wins by guessing all of the letters in the word before running out of guesses.

If you're just getting started with JavaScript and DOM manipulation, the instructions below will guide you. If you're more experienced and interested in designing the overall solution from scratch, skip the directions below and get started; you can delete all the function stubs in the JS, too.

Steps

* Write a function chooseWord that returns a random word from the words array.
* Write a function createWordTiles that takes a string as its argument, creates the div.letter tiles for each letter in the word, and places the tiles in div#word.
* Write a function validate that takes a string as its argument, and returns true if the guess is exactly one letter, and false otherwise.
* Write a function testGuess that takes a single-letter string as its argument. If the letter is in the target word, return an array containing the index (or indices) where the letter appears in the word. If the letter is not in the target word, return an empty array.
* Write a function updateWordTiles that takes an array of indices and updates the DOM to show the letter tiles at the specified indices.
* Write a function updateScoreboard that receives two arguments: the guess, and the number of incorrect guesses so far; it should the Incorrect Guesses and Guesses Remaining portion of the scoreboard.
* Write a function listen that listens for user input, then handles it using the functions you have created.
* Write a function init that uses the other functions you have created to set up and play the game.



# Tic Tac Toe (improve on current tic tac toe)
Use the table above as the game board. The state of the game is represented by an array of arrays:

let state = [
  [ 'x', 'o', null ],
  [ 'x', 'x', 'o' ],
  [ 'o', 'x', null ]
];
* Write the populate function to populate the table according to the game state.
* Write the nextPlayer function to determine which player should go next based on the state of the game, assuming that "x" always goes first. Return "x" or "o".
* Write the findWinner function to determine whether there is a winner in the current game. Return "x" or "o" if there is a winner, and null if there is not.
* Imagine a 4 x 4 board where a player wins by connecting three positions -- how would your solutions change?

How would your solutions change to adapt to arbitrary board sizes and arbitrary requirements for the number of positions that must connect? For example, consider an 8x8 board where four of a player's pieces must connect.


# Number-guessing

* Write the init function to set up an event listener on the form. The event listener should pass the value of the input element to the check function.
* Write the check function to accept a value from the event listener and check it against the targetNumber.
* If the values match, call the showWin function
* If the values do not match, call the showError function.
* If the values do not match, and the player has made more than five guesses, call the showLoss function.
* Write the showWin function to remove the form and any error message, and show a message telling the player they win.
* Write the showError function to show a message telling the player their guess is incorrect.
* Write the showLoss function to remove the form and show a message telling the player they lose.
